import 'package:clean_tdd/data/models/post_models.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:clean_tdd/domain/repositori/post_repository.dart';
import 'package:clean_tdd/domain/useCase/get_post.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../databases/database_reader.dart';
import 'get_post_test.mocks.dart';

//modified
@GenerateMocks([PostRepository])
void main() {
  late final GetPostUseCase usecase;
  late final MockPostRepository repository;

  setUp(() {
    repository = MockPostRepository();
    usecase = GetPostUseCase(postRepository: repository);
  });

  List<PostEntity> usersData = postFromJson(fixsure("post_database.json"));

  test("obtener lista de post del usuario", () async {
    when(repository.getPosts(userId: 1))
        .thenAnswer((_) async => Right(usersData));

    final result = await usecase.getPostUser(1);

    expect(result, Right(usersData));

    verify(repository.getPosts(userId: 1));
    verifyNoMoreInteractions(repository);
  });
}
