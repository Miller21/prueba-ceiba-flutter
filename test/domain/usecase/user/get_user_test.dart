import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/domain/repositori/user_repository.dart';
import 'package:clean_tdd/domain/useCase/get_user.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_user_test.mocks.dart';


//modified
@GenerateMocks([UserRepository])

void main() {
  late final GetUser usecase;
  late final MockUserRepository repository;

  setUp(() {
    repository = MockUserRepository();
    usecase = GetUser(repository: repository);
  });

  List<UserEntity> usersData = const [
    UserEntity(
        id: 1,
        name: "Miller Olaya",
        email: "miller@gmail.com",
        phone: "3104267148"),
    UserEntity(
        id: 2,
        name: "Daniela Sevillano",
        email: "sevillano@gmail.com",
        phone: "3225269902"),
  ];

  test("obtener lista de usuarios", () async {
    when(repository.getUsers()).thenAnswer((_) async => Right(usersData));

    final result = await usecase.getUsers();

    expect(result, Right(usersData));

    verify(repository.getUsers());
    verifyNoMoreInteractions(repository);
  });
}
