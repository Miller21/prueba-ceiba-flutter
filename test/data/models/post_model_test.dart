import 'package:clean_tdd/data/models/post_models.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../databases/database_reader.dart';

void main() {
  final post = postFromJson(fixsure("post_database.json"));

  test("subcripcion a la entidad post ", () async {
    expect(post, isA<List<PostEntity>>());
  });

  group("validar fpostFromJson =>", () {
    test("debe validar que el fromJson retorne una lista de postModel", () async {
     
      expect(post, isA<List<PostModel>>());
    });

    test("debe comparar los datos del fromjson", () async {
    
      expect(post, post);
    });

  });



}
