import 'package:clean_tdd/data/models/user_model.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../databases/database_reader.dart';

void main() {
  final users = userModelFromJson(fixsure("users.json"));

  test("subcripcion a la entidad user ", () async {
    expect(users, isA<List<UserEntity>>());
  });

  group("validar fromJson =>", () {
    test("debe validar que el fromJson retorne una lista de userModel", () async {
      final result = userModelFromJson(fixsure("users.json"));
      expect(result, isA<List<UserModel>>());
    });

    test("debe comparar los datos del fromjson", () async {
      final result = userModelFromJson(fixsure("users.json"));
      expect(result, users);
    });

  });



}
