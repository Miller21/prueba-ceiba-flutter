


import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/data/datasources/local/user/user_local_datasource.dart';
import 'package:clean_tdd/data/datasources/remote/user/user_remote_datasoirce.dart';
import 'package:clean_tdd/data/models/user_model.dart';
import 'package:clean_tdd/data/repositories/user/user_repositoty.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:dartz/dartz.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../databases/database_reader.dart';
import 'user_repositori_imple_test.mocks.dart';

//modified
@GenerateMocks([UserRemoteDatasource])
@GenerateMocks([NetworkInfo])
@GenerateMocks([UserLocalDatasource])


void main() {
  late UserRepositoryImpl userRepositoryImpl;
  late MockUserRemoteDatasource mockUserRemoteDatasource;
  late MockUserLocalDatasource mockUserLocalDatasource;
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockUserRemoteDatasource = MockUserRemoteDatasource();
    mockUserLocalDatasource = MockUserLocalDatasource();
    mockNetworkInfo = MockNetworkInfo();
    userRepositoryImpl = UserRepositoryImpl(
        userLocalDatasource: mockUserLocalDatasource,
        userRemoteDatasource: mockUserRemoteDatasource,
        networkInfo: mockNetworkInfo);
  });

 

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }


      group("obtener data desde la implementacion =>", () {
          List<UserModel> users = userModelFromJson(fixsure("users.json"));
          List<UserEntity> userEntityconvert = users;

        test("validar si el dispositivo esta en linea", () async {
          when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        });
       
       runTestsOnline(() {
        test("obtener data de la fuente local de manera exitosa", () async {
          // arrange
          when(mockUserLocalDatasource.getUsers())
              .thenAnswer((_) async => users);

          // act
          final result = await userRepositoryImpl.getUsers();

          // assert
          verifyNever(mockUserRemoteDatasource.getUsers());
          expect(result,
              equals(Right<Failure, List<UserEntity>>(userEntityconvert)));
        });
       });

     

        // este se ejecuta cuando no hay data almacenada de manera local
      runTestsOnline(() {
        test("obtener data de la fuente remota de manera exitosa", () async {
          // arrange
          when(mockUserRemoteDatasource.getUsers())
              .thenAnswer((_) async => users);

          // act
          final result = await userRepositoryImpl.getUsers();

          // assert
          verify(mockUserRemoteDatasource.getUsers());
          expect(result,
              equals(Right<Failure, List<UserEntity>>(userEntityconvert)));
        });


         // este se ejecuta cuando hay un error al traer los datos de manera remota
         test("obtener data de la fuente remota de manera fallida", () async {
           // arrange
           when(mockUserRemoteDatasource.getUsers())
               .thenThrow(const ServerException(message: "error"));

           // act
           final result = await userRepositoryImpl.getUsers();

           // assert
           verifyNever(mockUserRemoteDatasource.getUsers());
           expect(
               result,
               equals(const Left<Failure, List<UserEntity>>(
                   ServerFailure(message: "Error de conexion"))));
         });
      });

       });
    }
  

