//modified
import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/data/datasources/remote/post/post_remote_datasource.dart';
import 'package:clean_tdd/data/models/post_models.dart';
import 'package:clean_tdd/data/repositories/post/post_repository.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../databases/database_reader.dart';
import 'post_repositori_imple_test.mocks.dart';

@GenerateMocks([PostRemoteDataSource])
@GenerateMocks([NetworkInfo])
void main() {
  late PostRepositoryImple postRepositoryImpl;
  late MockPostRemoteDataSource mockPostRemoteDataSource;
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockPostRemoteDataSource = MockPostRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    postRepositoryImpl = PostRepositoryImple(
        postRemoteDataSource: mockPostRemoteDataSource,
        networkInfo: mockNetworkInfo);
  });

  final post = postFromJson(fixsure("post_database.json"));
  final List<PostEntity> postEntity = post;

  void runTestsOnline(Function body) {
    group("device is online", () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  group("obtener data desde la implementacion =>", () {
    test("validar si el dispositivo esta en linea", () async {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
    });

    runTestsOnline(() {
      test("debe llamar al metodo getPosts del datasource remoto", () async {
        // arrange
        when(mockPostRemoteDataSource.getPosts(1))
            .thenAnswer((_) async => post);

        // act
       final result = await postRepositoryImpl.getPosts(userId: 1);

        // assert
        verify(mockPostRemoteDataSource.getPosts(1));
        expect(result, equals(Right<Failure, List<PostEntity>>(postEntity)));
      });
    });
  });
}
