import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/presentation/routes/app_routes.dart';
import 'package:clean_tdd/presentation/screens/home/home_page.dart';
import 'package:clean_tdd/presentation/screens/post/post_page.dart';
import 'package:clean_tdd/presentation/screens/splash/splash_page.dart';
import 'package:flutter/material.dart';



final routes = <String, WidgetBuilder>{
  //Lista de rutas
  Routes.init: (_) => const SplashPage(),
  Routes.home: (_) =>  const HomePage(),
  Routes.postUser: (context) => PostPage(user: ModalRoute.of(context)!.settings.arguments as UserEntity) ,
  
};
