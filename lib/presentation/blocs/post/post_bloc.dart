
import 'package:bloc/bloc.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:clean_tdd/domain/useCase/get_post.dart';
import 'package:equatable/equatable.dart';

part 'post_event.dart';
part 'post_state.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final GetPostUseCase getPostUseCase;
  PostBloc({required this.getPostUseCase}) : super(PostInitial()) {
    on<PostFetched>((event, emit) async {
      emit(PostLoading());
      final post = await getPostUseCase.getPostUser(event.userId);
      post.fold(
        (error) => emit(PostError(message: error.message)),
        (success) => emit(PostLoaded(posts: success)),
      );
    });
  }
}
