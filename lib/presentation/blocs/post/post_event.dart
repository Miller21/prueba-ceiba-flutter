part of 'post_bloc.dart';

abstract class PostEvent extends Equatable {
  const PostEvent();

  @override
  List<Object> get props => [];
}

class PostFetched extends PostEvent {
  final int userId;

  const PostFetched({required this.userId});

  @override
  List<Object> get props => [userId];

}
