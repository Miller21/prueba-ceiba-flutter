

import 'package:bloc/bloc.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/domain/useCase/get_user.dart';
import 'package:equatable/equatable.dart';

part 'users_event.dart';
part 'users_state.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final GetUser getUser;
  UsersBloc({required this.getUser}) : super(UsersInitial()) {


    on<GetUsersEvent>((event, emit) async {
      emit(UsersLoading());
      final users = await getUser.getUsers();
      users.fold((error) {
        emit(UsersError(message: error.message));
      }, (success) {
        emit(UsersLoaded(users: success));
      });
    });

    on<SearchUsersEvent>((event, emit) async {
      
      if (event.query.isNotEmpty) {
       final users = await getUser.searchUser(query: event.query); 
        users.fold((error) {
          emit(UsersError(message: error.message));
        }, (success) {
          emit(UsersLoaded(users: success));
        });
      } else {
        final users = await getUser.getUsers();
        users.fold((error) {
          emit(UsersError(message: error.message));
        }, (success) {
          emit(UsersLoaded(users: success));
        });
      }
    });
  }
}
