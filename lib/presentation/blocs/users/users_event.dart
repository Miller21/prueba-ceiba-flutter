part of 'users_bloc.dart';

abstract class UsersEvent extends Equatable {
  const UsersEvent();

  @override
  List<Object> get props => [];
}

class GetUsersEvent extends UsersEvent {}

class SearchUsersEvent extends UsersEvent {
  final String query;
  const SearchUsersEvent({required this.query});
  @override
  List<Object> get props => [query];
}
