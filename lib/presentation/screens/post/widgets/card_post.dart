import 'package:clean_tdd/core/ui/tipografy.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:flutter/material.dart';

class CardsPost extends StatelessWidget {
  const CardsPost({
    required this.post,
    Key? key,
  }) : super(key: key);

  final PostEntity post;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      padding: const EdgeInsets.all(8.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text(post.title, style: styleTituloW300,),
              Text(post.body, maxLines: 4 ,overflow: TextOverflow.ellipsis,),
            ],
          ),
        ),
      ),
    );
  }
}