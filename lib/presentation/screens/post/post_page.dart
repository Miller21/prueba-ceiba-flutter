
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/presentation/blocs/post/post_bloc.dart';
import 'package:clean_tdd/presentation/screens/home/widgets/label_icon.dart';
import 'package:clean_tdd/presentation/screens/post/widgets/card_post.dart';
import 'package:clean_tdd/presentation/screens/widgets/card_user.dart';
import 'package:clean_tdd/presentation/screens/widgets/error_page.dart';
import 'package:clean_tdd/presentation/screens/widgets/loading_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostPage extends StatefulWidget {
  const PostPage({required this.user, super.key});
  final UserEntity user;

  @override
  State<PostPage> createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  @override
  void initState() {
    context.read<PostBloc>().add(PostFetched(userId: widget.user.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(
                height: 120,
                width: double.infinity,
                child: CardUser(
                  user: widget.user,
                  isPost: true,
                )),
            const LabelTextIcon(
                text: "Publicaciones", iconData: Icons.notes_outlined),
            Expanded(child: BlocBuilder<PostBloc, PostState>(
              builder: (context, state) {
                if (state is PostLoading) {
                  return const LoadinPage();
                }

                if (state is PostLoaded) {
                  return ListView.builder(
                      itemCount: state.posts.length,
                      itemBuilder: (context, index) {
                        if (state.posts.isEmpty) {
                          return const Center(
                            child: Text("List is empty"),
                          );
                        }

                        return CardsPost(
                          post: state.posts[index],
                        );
                      });
                }

                if (state is PostError) {
                  return ErrorPage(
                    message: state.message,
                    onPressed: () {
                      context
                          .read<PostBloc>()
                          .add(PostFetched(userId: widget.user.id));
                    },
                  );
                }
                return ErrorPage(
                  message: "Error desconocido",
                  onPressed: () {
                    context
                        .read<PostBloc>()
                        .add(PostFetched(userId: widget.user.id));
                  },
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
