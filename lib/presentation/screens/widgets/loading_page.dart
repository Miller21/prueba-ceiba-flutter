

import 'package:flutter/material.dart';

class LoadinPage extends StatelessWidget {
  const LoadinPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator()
    );
  }
}