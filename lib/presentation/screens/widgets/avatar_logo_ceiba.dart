
import 'package:clean_tdd/core/ui/colors.dart';
import 'package:flutter/material.dart';

class LogoCeiba extends StatelessWidget {
  const LogoCeiba({super.key});

  @override
  Widget build(BuildContext context) {
    return const CircleAvatar(
      backgroundColor: kPrimaryColor,
      backgroundImage:  AssetImage("assets/clogo.png"),
    );
  }
}