import 'package:clean_tdd/core/ui/tipografy.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/presentation/screens/home/widgets/label_icon.dart';
import 'package:flutter/material.dart';
import 'package:clean_tdd/presentation/routes/app_routes.dart';

class CardUser extends StatelessWidget {
  const CardUser({
    required this.user,
     this.isPost = false,
    Key? key,
  }) : super(key: key);
  final UserEntity user;
  final bool? isPost;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              user.name,
              style: styleTituloW300,
            ),
            LabelTextIcon(iconData: Icons.email, text: user.email),
            LabelTextIcon(iconData: Icons.phone_android, text: user.phone),
              
              isPost! ? Container() :
                 Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(
                              context, arguments: user, Routes.postUser);
                        },
                        child: const Text('Ver Publicaciones',style: styleTituloNormal)))
                
          ],
        ),
      ),
    );
  }
}
