


import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({super.key, required this.message, required this.onPressed});
  final Function onPressed;
  final String message;

  @override
  Widget build(BuildContext context) {
    return  Center(
      child: Column(
        children: [
          Text(message, textAlign: TextAlign.center,),

          ElevatedButton(
            onPressed: () => onPressed,
            child: const Text("Reintentar"),
          ) 
        ],
      )
    );
  }
}