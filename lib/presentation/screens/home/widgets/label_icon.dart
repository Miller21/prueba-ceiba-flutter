import 'package:clean_tdd/core/ui/tipografy.dart';
import 'package:flutter/material.dart';

class LabelTextIcon extends StatelessWidget {
  const LabelTextIcon({
    Key? key,
    required this.text,
    required this.iconData,
  }) : super(key: key);

  final String text;
  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(iconData),
        Text(text, style: styleTituloNormal,),
      ],
    );
  }
}