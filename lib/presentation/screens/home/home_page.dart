
import 'package:clean_tdd/core/utils/debouncer.dart';
import 'package:clean_tdd/presentation/blocs/users/users_bloc.dart';
import 'package:clean_tdd/presentation/screens/widgets/avatar_logo_ceiba.dart';
import 'package:clean_tdd/presentation/screens/widgets/card_user.dart';
import 'package:clean_tdd/presentation/screens/widgets/error_page.dart';
import 'package:clean_tdd/presentation/screens/widgets/loading_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Prueba ingreso'),
        leading: const Padding(
          padding: EdgeInsets.all(8.0),
          child: LogoCeiba(),
        ),
      ),
      body: BlocBuilder<UsersBloc, UsersState>(
        builder: (context, state) {
          if (state is UsersLoading) {
            return const LoadinPage();
          }

          if (state is UsersLoaded) {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: TextField(
                    textCapitalization: TextCapitalization.words,
                    decoration: const InputDecoration(
                      hintText: 'Buscar usuario',
                    ),
                    onChanged: (query) {
                    
                      final debouncer = Debouncer(milliseconds: 500);
                      debouncer.run(() {
                        context
                            .read<UsersBloc>()
                            .add(SearchUsersEvent(query: query));
                      });
                    },
                  ),
                ),
                state.users.isEmpty
                    ? const Center(
                        child: Text('List is empty'),
                      )
                    : Expanded(
                        child: ListView.builder(
                            itemCount: state.users.length,
                            keyboardDismissBehavior:
                                ScrollViewKeyboardDismissBehavior.onDrag,
                            itemBuilder: (context, index) {
                              return Container(
                                width: double.infinity,
                                height: 150,
                                padding: const EdgeInsets.all(8.0),
                                child: CardUser(user: state.users[index]),
                              );
                            }),
                      ),
              ],
            );
          }

          if (state is UsersError) {
            return ErrorPage(
                message: state.message,
                onPressed: () {
                  context.read<UsersBloc>().add(GetUsersEvent());
                });
          }

          return ErrorPage(
              message: "Error desconocido",
              onPressed: () {
                context.read<UsersBloc>().add(GetUsersEvent());
              });
        },
      ),
    );
  }
}
