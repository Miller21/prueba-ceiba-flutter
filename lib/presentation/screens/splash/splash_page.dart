import 'package:clean_tdd/core/ui/colors.dart';
import 'package:clean_tdd/presentation/routes/app_routes.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: FutureBuilder(
          future: _changePageSimulation(context),
          builder: (context, snapshot) {
            return Center(
              child: TweenAnimationBuilder(
                  tween: Tween<double>(begin: 0, end: 1),
                  duration: const Duration(milliseconds: 1500),
                  builder: (context, value, child) {
                    return AnimatedSize(
                      duration: const Duration(milliseconds: 1500),
                      child:
                          Image.asset("assets/clogo.png", height: 200 * value),
                    );
                  }),
            );
          }),
    );
  }
}

Future<void> _changePageSimulation(BuildContext context) async {
  // delay and navigate home route
  await Future.delayed(const Duration(milliseconds: 1800));
  // ignore: use_build_context_synchronously
  Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
}
