import 'package:clean_tdd/core/localDatabase/sembast_bd.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/data/datasources/local/user/user_local_datasource.dart';
import 'package:clean_tdd/data/datasources/local/user/users_local_implementation.dart';
import 'package:clean_tdd/data/datasources/remote/post/post_remote_datasource.dart';
import 'package:clean_tdd/data/datasources/remote/post/post_remote_datasource_impl.dart';
import 'package:clean_tdd/data/datasources/remote/user/user_remote_datasoirce.dart';
import 'package:clean_tdd/data/datasources/remote/user/user_remote_datasource_imple.dart';
import 'package:clean_tdd/data/repositories/post/post_repository.dart';
import 'package:clean_tdd/data/repositories/user/user_repositoty.dart';
import 'package:clean_tdd/domain/repositori/post_repository.dart';
import 'package:clean_tdd/domain/repositori/user_repository.dart';
import 'package:clean_tdd/domain/useCase/get_post.dart';
import 'package:clean_tdd/domain/useCase/get_user.dart';
import 'package:clean_tdd/presentation/blocs/post/post_bloc.dart';
import 'package:clean_tdd/presentation/blocs/users/users_bloc.dart';
import 'package:data_connection_checker_nulls/data_connection_checker_nulls.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final sl = GetIt.instance;
Future<void> init() async {
  // Blocs
  sl.registerFactory(() => UsersBloc(getUser: sl()));
  sl.registerLazySingleton(() => PostBloc(getPostUseCase: sl()));


  // Use cases
  sl.registerLazySingleton(() => GetUser(repository: sl()));
  sl.registerLazySingleton(() => GetPostUseCase(postRepository: sl()));

  // Repository
  sl.registerLazySingleton<UserRepository>(() => UserRepositoryImpl(
      userRemoteDatasource: sl(),
      networkInfo: sl(),
      userLocalDatasource: sl()));

  sl.registerLazySingleton<PostRepository>(() => PostRepositoryImple(
      postRemoteDataSource: sl(),
      networkInfo: sl(),
      )); 


  // Data sources
  sl.registerLazySingleton<UserRemoteDatasource>(
      () => UserRemoteDatasourceImpl(client: sl()));
  sl.registerLazySingleton<UserLocalDatasource>(
      () => UserLocalDatasourceImpl(sembasDB: sl()));
  
  sl.registerLazySingleton<PostRemoteDataSource>(
      () => PostRemoteDataSourceImple(client: sl()));    


  // Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  // External
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());

  // DB Sembast
  sl.registerLazySingleton<SembasDB>(() => SembasDBImplementation());
}
