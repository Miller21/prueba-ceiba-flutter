import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/core/utils/global_utils.dart';
import 'package:clean_tdd/data/datasources/remote/post/post_remote_datasource.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:clean_tdd/domain/repositori/post_repository.dart';
import 'package:dartz/dartz.dart';

class PostRepositoryImple implements PostRepository {
  final NetworkInfo networkInfo;
  final PostRemoteDataSource postRemoteDataSource;
  PostRepositoryImple(
      {required this.networkInfo, required this.postRemoteDataSource});

  @override
  Future<Either<Failure, List<PostEntity>>> getPosts(
      {required int userId}) async {
        
    return Utils.responseRepositoryTempl(
        method: postRemoteDataSource.getPosts,
        networkInfo: networkInfo,
        param: userId);
  }
}
