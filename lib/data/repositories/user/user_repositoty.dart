import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/errors/message_errors.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/data/datasources/local/user/user_local_datasource.dart';
import 'package:clean_tdd/data/datasources/remote/user/user_remote_datasoirce.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/domain/repositori/user_repository.dart';
import 'package:dartz/dartz.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDatasource userRemoteDatasource;
  final NetworkInfo networkInfo;
  final UserLocalDatasource userLocalDatasource;
  UserRepositoryImpl(
      {required this.userRemoteDatasource,
      required this.networkInfo,
      required this.userLocalDatasource});

  @override
  Future<Either<Failure, List<UserEntity>>> getUsers() async {
    try {
      final localUsers = await userLocalDatasource.getUsers();
      if (localUsers.isNotEmpty) {
        return Right(localUsers);
      }

      if (await networkInfo.isConnected) {
        final remoteUsers = await userRemoteDatasource.getUsers();
        userLocalDatasource.inserUsers(remoteUsers);
        return Right(remoteUsers);
      } else {
        return const Left(
          ServerFailure(
            message: erronInternet,
          ),
        );
      }
    } on ServerException catch (e) {
      return Left(ServerFailure(message: e.message));
    }
  }

  @override
  Future<Either<Failure, List<UserEntity>>> searchUser(
      {required String query}) async {
    try {
      final localUsers = await userLocalDatasource.searchUser(query);

      return Right(localUsers);
    } on ServerException catch (e) {
      return Left(CacheFailure(message: e.message));
    }
  }
}
