

import 'package:clean_tdd/data/models/post_models.dart';

abstract class PostRemoteDataSource {
  Future<List<PostModel>> getPosts(int userId);
}