


import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/utils/global_utils.dart';
import 'package:clean_tdd/data/datasources/remote/post/post_remote_datasource.dart';
import 'package:clean_tdd/data/models/post_models.dart';
import 'package:http/http.dart' as http;

class PostRemoteDataSourceImple implements PostRemoteDataSource {
  final http.Client client;
  PostRemoteDataSourceImple({required this.client});

  @override
  Future<List<PostModel>> getPosts(int userId) async {
    try {
      final response = await Utils.callGetClient(client, endpoint: "posts?userId=$userId");
      return Utils.responseDataSourceTempl<List<PostModel>>(
        response,
        data: postFromJson(response.body),
      );
    } catch (e) {
      throw ServerException(message: e.toString());
    }
  }
}