
import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/utils/global_utils.dart';
import 'package:http/http.dart' as http;
import 'package:clean_tdd/data/datasources/remote/user/user_remote_datasoirce.dart';
import 'package:clean_tdd/data/models/user_model.dart';

class UserRemoteDatasourceImpl implements UserRemoteDatasource {
  final http.Client client;
  UserRemoteDatasourceImpl({required this.client});

  @override
  Future<List<UserModel>> getUsers() async {
    try {
      final response = await Utils.callGetClient(client, endpoint: "users");
      return Utils.responseDataSourceTempl<List<UserModel>>(
        response,
        data: userModelFromJson(response.body),
      );
    } catch (e) {
      throw ServerException(message: e.toString());
    }
  }
}
