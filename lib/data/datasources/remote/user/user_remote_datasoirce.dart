
import 'package:clean_tdd/data/models/user_model.dart';

abstract class UserRemoteDatasource {
  // llama al enpoint https://jsonplaceholder.typicode.com/users.
  // lanza un [ServerException] para todos los codigo de error.
  
  Future<List<UserModel>> getUsers();
}