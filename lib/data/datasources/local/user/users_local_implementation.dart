
import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/localDatabase/sembast_bd.dart';
import 'package:clean_tdd/data/datasources/local/user/user_local_datasource.dart';
import 'package:clean_tdd/data/models/user_model.dart';

class UserLocalDatasourceImpl extends UserLocalDatasource {
  final SembasDB sembasDB;
  UserLocalDatasourceImpl({required this.sembasDB});

  @override
  Future<List<UserModel>> getUsers() async {
    try {
      final data = await sembasDB.getAllRecords("users");
      final users = data.map((e) => UserModel.fromJson(e)).toList();
      return users;
    } catch (e) {
      return [];
    }
  }

  @override
  Future<void> inserUsers(List<UserModel> users) async {
    try {
      final data = users.map((e) => e.toJson()).toList();
      
      for (var element in data) {
        await sembasDB.insertRecord("users", element);
      }

    }  catch (e) {
      throw CacheException(message: e.toString());
    }
  }
  
  @override
  Future<List<UserModel>> searchUser(String query) async {
    try {
      final data = await sembasDB.getAllRecords("users");
      final users = data.map((e) => UserModel.fromJson(e)).toList();
      final result = users.where((element) => element.name.contains(query)).toList();
      return result;
    } catch (e) {
      return [];
    }
     
  }
}
