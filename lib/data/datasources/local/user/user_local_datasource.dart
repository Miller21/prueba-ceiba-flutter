

import 'package:clean_tdd/data/models/user_model.dart';

abstract class UserLocalDatasource {
  
 Future<List<UserModel>> getUsers();  
 Future<List<UserModel>> searchUser(String query);  
 Future<void> inserUsers(List<UserModel> users);

}