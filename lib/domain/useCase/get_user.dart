

import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:clean_tdd/domain/repositori/user_repository.dart';
import 'package:dartz/dartz.dart';

class GetUser  {
  final UserRepository repository;
  GetUser({required this.repository});

   Future<Either<Failure, List<UserEntity>>> getUsers() async {
    return await repository.getUsers();
   } 

    Future<Either<Failure, List<UserEntity>>> searchUser({required String query}) async {
      return await repository.searchUser(query: query);
    }

}