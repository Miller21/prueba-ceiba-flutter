

import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:clean_tdd/domain/repositori/post_repository.dart';
import 'package:dartz/dartz.dart';

class GetPostUseCase {
  final PostRepository postRepository;

  GetPostUseCase({required this.postRepository});

  Future<Either<Failure, List<PostEntity>>>  getPostUser(int id) async {
    return await postRepository.getPosts(userId: id);
  }
}