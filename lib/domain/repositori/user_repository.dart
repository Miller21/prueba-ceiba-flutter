
import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/domain/entities/user_entity.dart';
import 'package:dartz/dartz.dart';

abstract class UserRepository {
  Future<Either<Failure, List<UserEntity>>> getUsers();
  Future<Either<Failure, List<UserEntity>>> searchUser({required String query});

}