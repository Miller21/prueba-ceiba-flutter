

import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/domain/entities/post_entity.dart';
import 'package:dartz/dartz.dart';

abstract class PostRepository {
  Future<Either<Failure, List<PostEntity>>> getPosts({required int userId});
}