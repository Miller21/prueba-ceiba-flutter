import 'dart:convert';

import 'package:clean_tdd/core/errors/exceptions.dart';
import 'package:clean_tdd/core/errors/failure.dart';
import 'package:clean_tdd/core/errors/message_errors.dart';
import 'package:clean_tdd/core/platform/network_info.dart';
import 'package:clean_tdd/core/utils/environment.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;



mixin Utils {
  static Future<T> responseDataSourceTempl<T>(
    http.Response response, {
    required T data,
  }) async {
    if (response.statusCode == 200) {
      return data;
    } else {
      throw ServerException(message: json.decode(response.body));
    }
  }

  static Future<Either<Failure, T>> responseRepositoryTempl<T>({
    required Function method,
    dynamic param,
   required NetworkInfo networkInfo,
  }) async {
    try {
      if ( await networkInfo.isConnected) {
        return Right(param == null ? await method() : await method(param));
      } else {
        return const Left(
          ServerFailure(
            message: erronInternet,
          ),
        );
      }
    } on ServerException catch (e) {
      return Left(ServerFailure(message: e.message));
    }
  }


  static Future<http.Response> callGetClient(
    http.Client client, {
    required String endpoint,
  }) async {
    try {
      final uri = Uri.parse("$urlApiBase/$endpoint");
      return await client.get(
        uri,
        headers: {
          "Content-Type": "application/json",
        },
      );
    } on http.ClientException catch (e) {
      throw ServerException(message: e.message);
    }
  }
}