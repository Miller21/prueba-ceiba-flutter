
import 'package:flutter/material.dart';

const styleTituloNormal = TextStyle(fontSize: 15, fontWeight: FontWeight.normal);
const styleTituloW300 =  TextStyle(fontSize: 18, fontWeight: FontWeight.bold, fontFamily: 'PoppinsBold');
const styleTituloW600 =  TextStyle(fontSize: 15, fontWeight: FontWeight.w600, fontFamily: 'PoppinsBold');
