import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:path_provider/path_provider.dart';

abstract class SembasDB {
  Future<Database> get db;
  Future<void> insertRecord(String storeName, Map<String, dynamic> user);
  Future<List<Map<String, dynamic>>> getAllRecords(String storeName);
}

class SembasDBImplementation implements SembasDB {
  @override
  Future<Database> get db async {
    return await openDB();
  }

  Future<Database> openDB() async {
    // obtener path de la base de datos SEMBAST
    var dir = await getApplicationDocumentsDirectory();
    var path = "${dir.path}/ceiba.db";

    DatabaseFactory dbFactory = databaseFactoryIo;

    final db = await dbFactory.openDatabase(path);
    return db;
  }

  Future<List<Map<String, dynamic>>> getRecords(
      Database db, String storeName) async {
    final store = intMapStoreFactory.store(storeName);
    final records = await store.find(db);
    return records.map((snapshot) {
      final record = snapshot.value;
      return record;
    }).toList();
  }

  @override
  Future<void> insertRecord(String storeName, Map<String, dynamic> user) async {
    final store = intMapStoreFactory.store(storeName);
    await store.add(await db, user);
  }

  @override
  Future<List<Map<String, dynamic>>> getAllRecords(String storeName) async {
    return await getRecords(await db, storeName);
  }
}
