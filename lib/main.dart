import 'package:clean_tdd/core/ui/colors.dart';
import 'package:clean_tdd/presentation/routes/app_routes.dart';
import 'package:clean_tdd/presentation/routes/app_screens.dart';
import 'package:flutter/material.dart';
import 'package:clean_tdd/injection_container.dart' as di;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:clean_tdd/presentation/blocs/users/users_bloc.dart';
import 'package:clean_tdd/presentation/blocs/post/post_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Inicializamos dependencias
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => di.sl<UsersBloc>()..add(GetUsersEvent())),
        BlocProvider(create: (_) => di.sl<PostBloc>()),
      ],
      child: MaterialApp(
        title: 'Material App',
        initialRoute: Routes.init,
        theme: ThemeData(
          fontFamily: 'Poppins',
        ).copyWith(
            colorScheme: const ColorScheme.light().copyWith(
          primary: kPrimaryColor,
          
        )),
        routes: routes,
      ),
    );
  }
}
