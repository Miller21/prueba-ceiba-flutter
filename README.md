## Prueba Ceiba Flutter

## ejecutar **flutter package get** para reinstalar dependencias del proyecto.


## Resultado

# Splash 
![plot](./assets/splashPage.jpeg)

Listar usuarios: Se debe consultar el servicio de listar
usuarios. se debe almacenar localmente.
Después de haber ingresado en la aplicación deberá
aparecer una lista con el nombre, email y teléfono del
usuario.

# Home page 
![plot](./assets/homepage.jpeg)

Filtrar lista de Usuarios: cada que se escribe una letra
debe ir mostrando los usuarios que su nombre
corresponda con el filtro, si el filtro no corresponde
con ningún usuario debe aparecer el mensaje “List is
empty”

# Search Page  
![plot](./assets/searchPage.jpeg)


Al seleccionar el botón “Ver publicaciones” se debe
mostrar el nombre, email, teléfono y publicaciones del
usuario.

# Post Page 
![plot](./assets/postPage.jpeg)

